import cv2
import pyautogui as pag
import numpy
import random
from random import uniform, choice, randint
import pytweening
import pytweening as PT
import time
from datetime import datetime
import math
import smtplib
import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from threading import Thread

'''
Commonly Used Functions
'''
tween_list = [PT.easeInOutSine,PT.easeInOutCirc,PT.easeInOutCubic,PT.easeInOutQuad,PT.easeInOutBounce,PT.easeInOutElastic]
username = 'halomage128'
password = 'ngnovakovic128'

def thread_process(function):
    mythread = Thread(target=function)
    mythread.run()

def send_email(Subject,Text,Recipient,attempt=0):
    try:
        fromaddr = "MoffittRPi@gmail.com"
        toaddr = Recipient
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        if '@' in Recipient:
            msg['To'] = Recipient
        else:
            while '@' not in Recipient:
                Recipient = raw_input('Please enter a valid email: ')
        msg['Subject'] = Subject
        body = Text
        msg.attach(MIMEText(body, 'plain'))
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login("MoffittRPi@gmail.com", "nicky128")
        text = msg.as_string()
        server.sendmail(fromaddr, Recipient, text)
        server.quit()
    except:
        att = attempt+1
        if att > 3:
            print("Failed to send email")
        else:
            print("Attempting to re-send...")
            send_email(Subject,Text,Recipient,attempt=att)

def click(c='left',dl=0.08,du=0.17):
    pag.click(button=c)
    alter = 0
    if chance(10):
        alter = uniform(0,dl)
    time.sleep(uniform(dl-alter,du+alter))

def chance(prob):
    '''
    1 in prob chance
    '''
    if random.randint(1,prob) == prob:
        return True
    else:
        return False
    
def duration_rand(t1,t2):
    t = uniform(t1,t2)
    return t

def location_rand(x1,y1,width,height):
    x = uniform(x1,x1+width)
    y = uniform(y1,y1+height)
    return (x,y)

def check_fighting(game_region = pag.size()):
    print("Check if fighting...")
    screen = current_region(region=game_region)
    # get region
    template = cv2.imread('./RockCrabs/Fight.png')
    res = cv2.matchTemplate(screen, template, cv2.TM_CCOEFF_NORMED)
    min_val,max_val,min_loc,max_loc = cv2.minMaxLoc(res)
    if max_val > 0.5:
        print("Still Fighting..")
        return True
    else:
        print("Done Fighting..Searching")
        return False
    
def health_check(location,health=1050):
    pag.screenshot('screen.png',region=(location))
    screen = cv2.imread('screen.png')
    count=0
    thr=60
    x = screen.shape[0]
    y = screen.shape[1]
    for px in range(x):
        for py in range(y):
            if screen[px,py,2] > thr:
                count+=1
    print(count)
    if count < health:
        print("Health below half...eating...")
        id_click('Lobster.png')
        
    
def inv(startx,starty,slot):
    x = startx+((slot%4)*50)
    y = starty+((int(slot/4))*45)
    x+=uniform(-1,1)
    y+=uniform(-1,1)
    return (x,y)

def recognized_regions(*regions):
    current_screen()
    test = cv2.imread('screen.png',1)
    pictures = [x for x in regions]
    for pic in pictures:
        x = int(pic[0]),int(pic[1])
        y = int(pic[0]+pic[2]),int(pic[1]+pic[3])
        cv2.rectangle(test,(x),(y),(0,255,0),3)
    test_img = cv2.imshow("Detection",test)
    cv2.waitKey(0)

def get_game_region():
    ul = get_location('UpperLeft.png')
    br = get_location('BottomRight.png')
    x = ul[0]
    y = ul[1]
    w = br[0]+br[3]-ul[0]
    h = br[1]+br[3]-ul[1]
    return x,y,w,h

def get_all_locations(image,region=(0,0,pag.size()[0],pag.size()[1])):
    coords = pag.locateAllOnScreen(image,region=region)
    return coords

def get_num_items(image,region=(0,0,pag.size()[0],pag.size()[1])):
    coords = pag.locateAllOnScreen(image,region=region)
    x = 0
    for coord in coords:
        x+=1
        print(coord)
    print("Found:",x,"items on screen")
    return x

def get_location(image,threshold=0.5,color=True,**args):
    if 'region' in args.keys():
        region = args['region']
        x = region[0]
        y = region[1]
        w = region[2]
        h = region[3]
    else:
        region = pag.size()
        x = 0
        y = 0
        w = region[0]
        h = region[1]
        region = (x,y,w,h)
    current_region((x,y,w,h))
    if not color:
        screen = cv2.imread('screen.png',0)
        temp = cv2.imread(image,0)
    else:
        screen = cv2.imread('screen.png')
        temp = cv2.imread(image)
    res = cv2.matchTemplate(screen, temp, cv2.TM_CCOEFF_NORMED)
    min_val,max_val,min_loc,max_loc = cv2.minMaxLoc(res)
    if max_val < threshold:
        print('Less than Threshold: Max: ',max_val)
        threshold-=0.1
        return get_location(image,threshold=threshold,region=region)
    else:
        imgsize = temp.shape
        width = imgsize[1]
        height = imgsize[0]
        y1=max_loc[1]
        x1=max_loc[0]
        return (x1+x,y1+y,width,height)

def is_in_image(screen,image,threshold=0.5,color=False):
    if not color:
        screen = cv2.imread(screen,0)
        temp = cv2.imread(image,0)
    else:
        screen = cv2.imread(screen)
        temp = cv2.imread(image)
    res = cv2.matchTemplate(screen, temp, cv2.TM_CCOEFF_NORMED)
    min_val,max_val,min_loc,max_loc = cv2.minMaxLoc(res)
    if max_val < threshold:
        return False
    else:
        return True

def get_contour_location(image):
    print("Contours")

def find_bank():
    loc = get_location('Banker.png')
    #bank_loc = loc[0]+(loc[2]/5),loc[1]+(loc[3]/5)
    bank_loc = loc[0],loc[1],loc[2],loc[3]
    return bank_loc

def find_inv_start():
    loc = get_location('Inv.png')
    inv_loc = loc[0]+65,loc[1]+70
    return inv_loc

def find_game_bounds(delay=0):
    '''
    returns the x,y,w,h of the game window
    '''
    time.sleep(delay)
    top_left = get_location('UpperLeft.png',color=False)
    bottom_right = get_location('BottomRight.png')
    tl = (top_left[0],top_left[1]+top_left[3])
    br = (bottom_right[0]+bottom_right[2], bottom_right[1]+bottom_right[3])
    w = br[0]-tl[0]
    h = br[1]-tl[1]
    x,y = tl
    return (x,y,w,h)

def get_rect(x,y,w,h):
    x1 = x-w
    y1 = y-h
    w1 = w*2
    h1 = h*2
    return (x1,y1,w1,h1)

def inner_75(x,y,w,h):
    x1 = x+(w*0.20)
    y1 = y+(h*0.20)
    w1 = w*0.6
    h1 = h*0.6
    return (x1,y1,w1,h1)

def get_center(x,y,w,h):
    x = x+w/2
    y = y+h/2
    return x,y

def distance(x1,y1,x2,y2):
    xdist = (x1-x2)**2
    ydist = (y1-y2)**2
    hyp = math.sqrt(xdist+ydist)
    return hyp

def face_north(compass_loc):
    x,y,w,h = compass_loc
    move(x,y,w,h)
    click()

def movement_randomizer(finish,duration):
    start = pag.position()
    step = random.randint(1,4)
    pytweening.getLine(start[0],start[1],finish[0],finish[1])
    hyp = distance(start[0],start[1],finish[0],finish[1])
    ### randomly scramble duration one in 10 times ###
    if chance(6):
        scramble = random.uniform(-0.25,0.25)
        duration+=scramble
        if duration < 0.1:
            duration = 0.1
    for i in range(step):
        s = float(i)/float(step)
        point = pytweening.getPointOnLine(start[0],start[1],finish[0],finish[1],s)
        ### Make as product of distance traveled ###
        changer = uniform(250,1000)
        #changer = uniform(5,100)
        mult = hyp/changer
        #print(changer,mult)
        alterx = random.uniform(5*mult,15*mult) # Change each point
        altery = random.uniform(5*mult,15*mult)
        x = point[0]+alterx
        y = point[1]+altery
        cx = pag.position()[0]
        cy = pag.position()[1]
        dist = distance(cx,cy,x,y)
        duration_mod = uniform(0.75,1.25)
        try:
            interval = ((dist/hyp)*duration)*duration_mod
        except:
            interval = 0.2
        #print("Movement Interval:",interval)
        pag.moveTo(x, y, interval, tween=choice(tween_list))
        
    ### Sometimes miss the final destination by X Pixels and reajust ###
    dist = distance(cx,cy,x,y)
    try:
        interval = (dist/hyp)*duration
    except:
        interval = 0.2
    #interval = duration/(step+1)
    if chance(3):
        # Overshoot...
        change = randint(-10,10)
        pag.moveTo(finish[0]+change, finish[1]+change, interval, tween=choice(tween_list))
        pag.moveTo(finish[0], finish[1], interval, tween=choice(tween_list))
    else:
        pag.moveTo(finish[0], finish[1], interval, tween=choice(tween_list))

# Edit pass to GET LOCATION instead of GIVE
def id_click(image,thr=0.5,c='left'):
    #screen = current_screen()
    #template = cv2.imread(image)
    #x1,y1,width,height = give_location(screen,template,threshold=thr)
    x1,y1,width,height = get_location(image,threshold=thr)
    width*=0.6
    height*=0.6
    x1=x1+(width/5)
    y1=y1+(height/5)
    move(x1,y1,width,height)
    click(c=c)
    
def logout():
    id_click('Logout.png')
    time.sleep(uniform(0.5,1))
    id_click('Close.png')
    
def login():
    current_screen()
    time.sleep(1)
    if is_in_image('screen.png','User.png',threshold=0.7):
        id_click('User.png')
        time.sleep(uniform(1,2))
        for letter in username:
            pag.typewrite(letter,interval=uniform(0.1,0.2))
        pag.press('tab')
        for letter in password:
            pag.typewrite(letter,interval=uniform(0.1,0.2))
        id_click('Login.png')
        time.sleep(uniform(5,7))
        id_click('Play.png')
        time.sleep(uniform(1,2))
        for i in range(len(password)):
            pag.press('backspace')
    else:
        print("Login screen not detected....Shutting down")

def check_stats(invx,invy):
    pag.press('f2')
    time.sleep(uniform(2,3))
    slot = randint(1,27)
    x,y = inv(invx, invy,slot)
    move(x,y,5,5)
    time.sleep(uniform(2,3))
    pag.press('escape')
    slot = randint(1,27)
    x,y = inv(invx, invy,slot)
    move(x,y,15,15)

def move(x1,y1,width,height,min_dur=0.14,max_dur=0.27):
    current = pag.position()
    cx = current[0]
    cy = current[1]
    random_location = location_rand(x1,y1,width,height)
    rx = random_location[0]
    ry = random_location[1]
    dx = abs(cx-rx)
    dy = abs(cy-ry)
    dist = math.sqrt((dx**2) + (dy**2))
    #min_dur = 0.1+(math.log10(1+(dist/1000)))
    #max_dur = min_dur+0.2
    #pag.moveTo(random_location,duration=duration_rand(min_dur,max_dur),tween=choice(tween_list))
    movement_randomizer(random_location,duration_rand(min_dur,max_dur))

def withdraw_amount(Bankx,Banky,item1,item2,in_bank=False):
    if in_bank:
        print("Already in bank")
    else:
        move(Bankx,Banky,40,40,min_dur=0.23,max_dur=0.36) # Bank
        click(dl=0.15,du=0.25)
    #time.sleep(uniform(0.15,0.62))
    id_click(item1,c='right')
    id_click('./Fletcher/Withdraw14.png')
    id_click(item2,c='right')
    id_click('./Fletcher/Withdraw14.png')
    id_click('./Enchanter/Exit.png')

def deposit_all(Bankx,Banky):
    move(Bankx,Banky,40,40,min_dur=0.23,max_dur=0.36) # Bank
    click(dl=0.15,du=0.28)
    id_click('DepAll.png')
    
def deposit(Bankx,Banky,Invx,Invy):
    move(Bankx,Banky,40,40,min_dur=0.23,max_dur=0.36) # Bank
    click(dl=0.15,du=0.28)
    #time.sleep(uniform(0.52,0.97))
    ri = randint(1,6)
    x,y = inv(Invx,Invy,ri) # Inv
    move(x,y,0,0,min_dur=0.25,max_dur=0.35)
    time.sleep(uniform(0.55,0.8))
    click(c='right',dl=0.15,du=0.18)
    id_click('./Enchanter/Deposit.png')
    
def bank(Bankx,Banky,Invx,Invy,item,wd=True):
    bank_img = cv2.imread('Banker.png')
    w,h,o = bank_img.shape
    move(Bankx,Banky,w-(w/5),h-(h/5),min_dur=0.35,max_dur=0.55) # Bank
    click(dl=1.5,du=2)
    #time.sleep(uniform(0.52,0.97))
    ri = randint(1,6)
    x,y = inv(Invx,Invy,ri) # Inv
    move(x,y,0,0,min_dur=0.25,max_dur=0.35)
    time.sleep(uniform(0.55,0.8))
    click(c='right',dl=0.15,du=0.18)
    id_click('./Enchanter/Deposit.png')
    if wd:
        id_click(item,c='right')
        id_click('./Enchanter/Withdraw_All.png')
    id_click('./Enchanter/Exit.png')

def withdraw(Bankx,Banky,item):
    bank_img = cv2.imread('Banker.png')
    w,h,o = bank_img.shape
    move(Bankx,Banky,w-(w/5),h-(h/5),min_dur=0.35,max_dur=0.55) # Bank
    click(dl=1.5,du=2)
    #time.sleep(uniform(0.15,0.62))
    id_click(item,c='right')
    id_click('./Enchanter/Withdraw_All.png')
    id_click('./Enchanter/Exit.png')
    
def move_item_in_inv(Invx,Invy,start_pos,end_pos):
    '''
    Drag an inventory item from one spot to another
    '''
    start = inv(Invx,Invy,start_pos)
    end = inv(Invx,Invy,end_pos)
    move(start[0],start[1],0,0)
    pag.dragTo(end[0],end[1],duration=uniform(0.45,0.85))

    
def current_region(region):
    pag.screenshot('screen.png',region=region)
    screen = cv2.imread('screen.png')
    return screen

def current_screen():
    pag.screenshot('screen.png')
    screen = cv2.imread('screen.png')
    return screen

def get_time():
    time = datetime.now().strftime('%Y-%m-%d--%H-%M-%S')
    return time

def random_right_clicks(game_region,clicks = 3):
    #bounds = get_location("GameWindow.png") # Gets the region of the game
    bounds = game_region
    for random_click in range(clicks):
        mod = uniform(-0.8,0.8)
        t = uniform(1.5+mod,2+mod)
        x,y,w,h = bounds
        move(x,y,w,h)
        click(c='right',dl=0.25,du=t)
    move(x,y,w,h)
    print("Finished random right clicks")

def random_mouse_movements(moves=3):
    bounds = (0, 0, 1000, 1000)
    for random_click in range(moves):
        mod = uniform(-0.8, 0.8)
        t = uniform(1.5 + mod, 2 + mod)
        x, y, w, h = bounds
        move(x, y, w, h)
        time.sleep(t)
    print("Finished random mouse moves")

def random_camera_movements(moves=3,min_dur=0.8,max_dur=1.4):
    keys = ['up','down','left','right']
    for move in range(moves):
        sleeptime = uniform(min_dur,max_dur)
        ct = time.time()
        key = random.choice(keys)
        pag.keyDown(key)
        dt = time.time()
        while dt-ct < sleeptime:
            dt = time.time()
            continue
        pag.keyUp(key)
        
def duration(start_time):
    current = time.time()
    change = current-start_time
    minutes = int(change/60)
    return minutes
### This checkpoint needs to surround the player not the player region....
### Get center point of player and then expand outward

def check_random(player_region):
    current_region(player_region) # Take screenshot
    x,y,w,h = player_region
    cx = x+(w/2)
    cy = y+(h/2)
    # Trying to center and get new coordinates
    x = cx-75
    y = cy-75
    w = 150
    h = 150
    is_random = is_in_image('screen.png','Ploxalot.png',threshold=0.6,color=False)
    if is_random:
        ctime = get_time()
        pag.screenshot('random_event'+ctime+'.png')
        send_email('Random Found','Get to your computer','ngimbrone@mail.usf.edu')
        for x1 in range(0,3):
            xinc = w/2
            xval = xinc*x1
            for y1 in range(0,3):
                yinc = h/2
                yval = yinc*y1
                print(x,xval,y,yval)
                xloc = x+xval
                yloc = y+yval
                move(xloc,yloc,2,2)
                y_move = randint(-150,150)
                x_move = randint(-150,150)
                click(c='right')
                newx,newy = pag.position()
                new_region = get_rect(newx,newy,300,300) #300 pixels around mouse
                pag.screenshot('random_screen.png',region=new_region)
                is_dismiss = is_in_image('random_screen.png','Dismiss.png',threshold=0.6)
                if is_dismiss:
                    id_click('Dismiss.png')
                    print('Random dismissed...')
                    send_email('Random Found','Dismissed Properly','ngimbrone@mail.usf.edu')
                else:
                    print('Looking for dismissal...')
                move(xloc+x_move,yloc+y_move,10,10)
        return True
    else:
        return False
                
                

if __name__ == '__main__':
    #Testing Scripts
    #random_right_clicks(clicks = 3)
    print('Running')

