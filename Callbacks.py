import pynput
from pynput import mouse, keyboard
from pynput.keyboard import Controller
from Common_Functions import *
import cv2
import pyautogui as pag
import numpy
import random
from random import uniform, choice, randint
import pytweening
import pytweening as PT
import time
import math
from Bot import Bot


'''
loop=[[895, 338, 1.5631330013275146, 'left'], [961, 339, 0.9066269397735596, 'left'], [1098, 66, 1.2974538803100586, 'left'], [1515, 352, 1.4152007102966309, 'left'], [710, 931, 6.360543966293335, 'left'], [442, 775, 16.687825918197632, 'left'], [1044, 789, 5.895938396453857, 'left'], [898, 342, 2.072004556655884, 'left'], [961, 342, 1.029426097869873, 'left'], [1095, 67, 1.5432748794555664, 'left'], [1512, 348, 1.4052000045776367, 'left'], [711, 932, 6.0383312702178955, 'left'], [442, 771, 16.9039888381958, 'left'], [1043, 794, 5.8941380977630615, 'left'], [0, 0, 1.3835139274597168, 'wait']]
loop=[[721, 341, 1.783154010772705, 'left'], [1093, 64, 1.8456270694732666, 'left'], [1325, 692, 1.1519577503204346, 'left'], [1373, 686, 0.7043092250823975, 'left'], [909, 931, 1.789797306060791, 'left'], [966, 575, 49.30227971076965, 'left'], [1378, 687, 1.9507067203521729, 'left'], [0, 0, 1.1549551486968994, 'wait']]
loop=[[987, 340, 1.5476322174072266, 'left'], [1058, 71, 1.3646152019500732, 'left'], [1500, 627, 1.448962688446045, 'left'], [1333, 756, 1.168226957321167, 'left'], [966, 621, 4.729951858520508, 'left'], [1373, 639, 1.986966609954834, 'left'], [1290, 687, 1.166872501373291, 'left'], [852, 128, 1.159745454788208, 'left'], [1452, 957, 33.152146339416504, 'left'], [930, 584, 4.0725226402282715, 'left'], [1300, 686, 2.572556257247925, 'left'], [983, 345, 2.8880245685577393, 'left'], [1059, 72, 1.221189022064209, 'left'], [1496, 632, 1.1618506908416748, 'left'], [1336, 758, 1.4148309230804443, 'left'], [1374, 626, 4.128609895706177, 'left'], [972, 623, 1.4763669967651367, 'left'], [1293, 689, 1.9204981327056885, 'left'], [850, 123, 1.4475328922271729, 'left'], [1448, 960, 32.06197738647461, 'left'], [929, 596, 4.769887924194336, 'left'], [1291, 690, 1.798755407333374, 'left'], [0, 0, 1.1225857734680176, 'wait']]
loop=[[798, 341, 1.4784042835235596, 'left'], [860, 335, 1.1620237827301025, 'left'], [997, 63, 1.224480390548706, 'left'], [1470, 314, 1.4154088497161865, 'left'], [536, 923, 5.863115549087524, 'left'], [273, 792, 17.054590940475464, 'left'], [943, 788, 5.417921304702759, 'left'], [796, 336, 1.573448896408081, 'left'], [862, 338, 1.1923494338989258, 'left'], [997, 61, 1.6732690334320068, 'left'], [1462, 312, 1.0560598373413086, 'left'], [526, 938, 6.239274024963379, 'left'], [271, 793, 17.29479694366455, 'left'], [948, 785, 6.160370826721191, 'left'], [0, 0, 1.038032054901123, 'wait']]
loop = [[621, 336, 1.5185649394989014, 'left'], [997, 69, 1.3483941555023193, 'left'], [1301, 681, 0.7787895202636719, 'left'], [1357, 683, 0.6490399837493896, 'left'], [723, 920, 1.2825849056243896, 'left'], [865, 578, 49.35377860069275, 'left'], [1361, 682, 2.0522310733795166, 'left'], [0, 0, 0.9210410118103027, 'wait']]
loop=[[918, 337, 1.3809106349945068, 'left'], [997, 66, 1.1489362716674805, 'left'], [1507, 627, 1.2061693668365479, 'left'], [1349, 755, 1.3826932907104492, 'left'], [899, 611, 5.080646991729736, 'left'], [1384, 629, 1.2706248760223389, 'left'], [1304, 683, 1.19089674949646, 'left'], [771, 85, 1.782259464263916, 'left'], [1304, 950, 32.659722089767456, 'left'], [870, 591, 5.3222432136535645, 'left'], [1305, 684, 2.0406782627105713, 'left'], [0, 0, 1.3099195957183838, 'wait']]
loop=[[857, 474, 1.7589941024780273, 'left'], [997, 63, 1.279078722000122, 'left'], [1301, 673, 0.9128696918487549, 'left'], [976, 594, 1.0129413604736328, 'left'], [528, 947, 1.0220701694488525, 'left'], [902, 428, 8.45523977279663, 'right'], [848, 484, 0.8219974040985107, 'left'], [940, 795, 3.2153592109680176, 'left'], [860, 466, 1.6565611362457275, 'left'], [996, 62, 1.5217902660369873, 'left'], [1301, 677, 1.158848524093628, 'left'], [966, 586, 1.1495707035064697, 'left'], [533, 925, 1.5283257961273193, 'left'], [898, 421, 8.266815900802612, 'right'], [872, 471, 0.7760775089263916, 'left'], [942, 781, 1.1484758853912354, 'left'], [0, 0, 1.2772746086120605, 'wait']]
loop=[[871, 577, 1.665879726409912, 'left'], [799, 337, 2.163187265396118, 'left'], [858, 334, 0.7756645679473877, 'left'], [995, 64, 1.4410500526428223, 'left'], [1481, 321, 1.2590880393981934, 'left'], [528, 930, 5.949514150619507, 'left'], [286, 794, 17.071391344070435, 'left'], [940, 782, 5.932105302810669, 'left'], [796, 337, 2.189460277557373, 'left'], [860, 340, 0.9590892791748047, 'left'], [995, 66, 1.6615779399871826, 'left'], [1470, 318, 1.273932933807373, 'left'], [531, 920, 5.590735197067261, 'left'], [283, 787, 17.2030348777771, 'left'], [940, 788, 5.8622448444366455, 'left'], [0, 0, 1.0535764694213867, 'wait']]
loop=[[619, 339, 1.231919288635254, 'left'], [996, 64, 1.389472484588623, 'left'], [1302, 684, 1.3045425415039062, 'left'], [1363, 687, 0.8239307403564453, 'left'], [801, 932, 1.3876276016235352, 'left'], [875, 579, 48.74692368507385, 'left'], [1359, 682, 2.1433918476104736, 'left'], [0, 0, 0.9732332229614258, 'wait']]


time.sleep(2)
count = 0
st = time.time()
duration = time.time()-st
while duration < 4500:
    duration = time.time()-st
    print(duration)
    count+=1
    start = time.time()
    for spot in loop:
        rx = randint(-2,2)
        ry = randint(-2,2)
        x = spot[0]-rx
        y = spot[1]-ry
        dur = spot[2]
        click_type = spot[3]
        if click_type == 'wait':
            time.sleep(dur)
        else:
            time.sleep(dur)
            move(x,y,0,0)
            click(c=click_type)
        #check_random(self.player_region)
    print("Lap:",time.time()-start)
'''

class Play():
    def __init__(self, storage, duration):
        self.storage = storage
        self.duration = duration
        print('Starting run in 3...')
        time.sleep(1)
        print('2...')
        time.sleep(1)
        print('1...')
        time.sleep(1)
        self.run()
        
        
    def run(self):
        count = 0
        st = time.time()
        duration = time.time()-st
        while duration < self.duration:
            duration = time.time()-st
            print(duration)
            count+=1
            start = time.time()
            for spot in self.storage:
                rx = randint(-2,2)
                ry = randint(-2,2)
                x = spot[0]-rx
                y = spot[1]-ry
                dur = spot[2]
                click_type = spot[3]
                if click_type == 'wait':
                    time.sleep(dur)
                else:
                    time.sleep(dur)
                    move(x,y,0,0)
                    click(c=click_type)
                #check_random(self.player_region)
            print("Lap:",time.time()-start)

class Map():
    def __init__(self):
        self.keys = Controller()
        with keyboard.Listener(on_press=self.on_press,on_release=self.on_release) as listener:
            listener.join()

    def on_press(self,key):
        if key == keyboard.KeyCode(char='w'):
            self.keys.press(keyboard.Key.up)
        if key == keyboard.KeyCode(char='a'):
            self.keys.press(keyboard.Key.left)
        if key == keyboard.KeyCode(char='s'):
            self.keys.press(keyboard.Key.down)
        if key == keyboard.KeyCode(char='d'):
            self.keys.press(keyboard.Key.right)
        if key == keyboard.KeyCode(char='1'):
            self.mage_prayer()
            self.change_gear()
        if key == keyboard.KeyCode(char='2'):
            self.range_prayer()
            self.change_gear()
        if key == keyboard.KeyCode(char='3'):
            self.change_gear()

    def on_release(self,key):
        if key == keyboard.KeyCode(char='w'):
            self.keys.release(keyboard.Key.up)
        if key == keyboard.KeyCode(char='a'):
            self.keys.release(keyboard.Key.left)
        if key == keyboard.KeyCode(char='s'):
            self.keys.release(keyboard.Key.down)
        if key == keyboard.KeyCode(char='d'):
            self.keys.release(keyboard.Key.right)

    def mage_prayer(self):
        print('praying mage')
        #hit f3 hit prayers
        self.keys.press(keyboard.Key.f3)
        pag.moveTo(1694,775,0.1)
        click(dl=0.02,du=0.05)
        pag.moveTo(1834,775,0.05)
        click(dl=0.02,du=0.05)
        self.keys.press(keyboard.Key.f2)
        
    def range_prayer(self):
        print('praying range')
        self.keys.press(keyboard.Key.f3)
        pag.moveTo(1648,819,0.1)
        click(dl=0.02,du=0.05)
        pag.moveTo(1742,775,0.05)
        click(dl=0.02,du=0.05)
        self.keys.press(keyboard.Key.f2)

    def change_gear(self):
        print('changing gear')
        self.keys.press(keyboard.Key.f2)
        pos = [[1665, 639],[1667, 674],[1719, 636],[1718, 677],[1764, 632],[1764, 679]]
        for p in pos:
            ri = uniform(0.01,0.03)
            rx = random.randint(-3,3)
            ry = random.randint(-3,3)
            x = p[0]+rx
            y = p[1]+ry
            pag.moveTo(x,y,ri)
            pag.click(button='left')
        
        
            
class Record():
    def __init__(self):
        self.store = False
        #self.store = True
        self.Storage = []
        
        with mouse.Listener(on_click=self.on_click,on_scroll=self.on_scroll) as listener:
            listener.join()
        
        #with mouse.Listener(on_click=self.on_click) as listener:
        #    listener.join()
        
            
    def on_click(self,x,y,button,pressed):
        if pressed == True:
            if self.store:
                if str(button) == 'Button.left':
                    click = 'left'
                if str(button) == 'Button.right':
                    click ='right'
                print("Clicked",x,y,button,pressed)
                position = [x,y]
                elapsed = time.time() - self.start_time
                self.start_time = time.time()
                self.Storage.append([x,y,elapsed,click])


    def on_scroll(self,x,y,dx,dy):
        if self.store:
            self.store = False
            elapsed = time.time() - self.start_time
            self.Storage.append([0,0,elapsed,'wait'])
            print(self.Storage)
            return False
        else:
            print("Starting")
            self.store = True
            self.Storage = []
            self.start_time = time.time()

if __name__ == '__main__':
    record = False
    while not record:
        user = input('Type --- "1" and hit Enter to Record.  Use mouse wheel to begin and end recording\n-----> ')
        if str(user) == '1':
            recorder = Record()
            storage = recorder.Storage
            record = True
            duration = int(input('Runtime duration in seconds: '))
            player = Play(storage, duration)
        
