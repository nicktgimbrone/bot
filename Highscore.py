import urllib.request as urllib2
from bs4 import BeautifulSoup

Skills = ['Total','Attack','Defence','Strength','Hitpoints','Range','Prayer',
          'Magic','Cooking','Woodcutting','Fletching','Fishing','Firemaking',
          'Crafting','Smithing','Mining','Herblore','Agility','Thieving',
          'Slayer','Farming','Runecrafting','Hunter','Construction']
Info = ['Rank','Level','Experience']


def highscore(username):
    Stats = {}
    user = '%20'.join(username.split(' '))
    hspage = urllib2.urlopen("http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player={:}".format(user))
    hssoup = str(BeautifulSoup(hspage,'html.parser'))
    stat = list(hssoup.split('\n'))
    for num in range(len(Skills)):
        skill = stat[num]
        skill = skill.split(',')
        skill_name = Skills[num]
        Stats[skill_name] = {}
        Stats[skill_name]['Rank'] = skill[0]
        Stats[skill_name]['Level'] = skill[1]
        Stats[skill_name]['Exp'] = skill[2]
    display(Stats)
    return Stats
        
def display(Stats):
    string = '{:<15} {:<5} {:<12}'.format('Skill','Lvl','Exp')
    print(string)
    for skill in Stats.keys():
        info = Stats[skill]
        string = '{:<15} {:<5} {:<12}'.format(skill,info['Level'],info['Exp'])
        print(string)

def exp_gain(D1,D2):
    changes = []
    for skill in Skills:
        exp = int(D2[skill]['Exp']) - int(D1[skill]['Exp'])
        if exp > 0:
            changes.append([skill,exp])
    return changes
'''
username = input("Type Username:" )
highscore(username)
'''
