from Common_Functions import *
from Highscore import *

class Bot(object):
    def __init__(self):
        print("Starting Bot")
        
    def screen(self):
        self.health_location = get_location('HealthBar.png',threshold=0.6,color=False)
        self.prayer_location = get_location('Prayer.png',threshold=0.6,color=False)
        self.inventory_start = find_inv_start()
        #self.invx,self.invy = self.inventory_start
        self.screen_bounds = find_game_bounds(delay=1)
        self.player_loc = get_center(*self.screen_bounds)
        self.player_region = get_rect(self.player_loc[0],self.player_loc[1],200,200)
        self.compass_loc = get_location('Compass.png',threshold=0.5)
        self.start_time = time.time()
        self.x_factor,self.y_factor = pag.size()
        self.x_factor = 1920/self.x_factor
        self.y_factor = 1080/self.y_factor
        #self.start_stats = highscore('sir ploxalot')
        # Get compass...

    def display(self):
        test = cv2.imread('screen.png',1)
        pictures = self.health_location,self.player_region,self.screen_bounds,self.compass_loc,self.prayer_location
        for pic in pictures:
            x = int(pic[0]),int(pic[1])
            y = int(pic[0]+pic[2]),int(pic[1]+pic[3])
            cv2.rectangle(test,(x),(y),(0,255,0),3)
        test_img = cv2.imshow("Detection",test)
        cv2.moveWindow("Detection",0,0)
        cv2.waitKey(0)
        cv2.destroyWindow("Detection")

    def display_coord(self,x,y,w,h):
        c1 = (x,y)
        c2 = (x+w,y+h)
        test = cv2.imread('screen.png',1)
        cv2.rectangle(test,c1,c2,(0,255,0),3)
        test_img = cv2.imshow("Detection",test)
        cv2.moveWindow("Detection",0,0)
        cv2.waitKey(0)
        cv2.destroyWindow("Detection")

    def clean_up(self):
        print("Logging out and sending stats")
        self.end_time = time.time()
        self.time_change = self.end_time-self.start_time
        logout()
        time.sleep(5)
        self.final_stats = highscore('sir ploxalot')
        changes = exp_gain(self.start_stats,self.final_stats)
        string = 'Stat Changes\n'
        for skill,exp in changes:
            try:
                exp_rate = (int(exp)/(float(self.time_change)/3600))
                exp_rate = int(exp_rate)
                string+=str(skill)+':'+str(exp)+' ----- ' +'Rate:'+str(exp_rate)+'exp/hour'+'\n'
            except:
                print(skill)
        send_email("Experience Report",string,"ngimbrone@mail.usf.edu")

        
